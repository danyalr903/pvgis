﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using RestSharp;
using System.Net;
using Newtonsoft.Json;
using geoCoding;
using System.Globalization;
using System.IO;
using System.Xml;
using System.Text;
using System.Web.Http;
using System.Web.Http.Cors;
namespace Service
{
    ///<summary>
    /// Summary description for PVGIS
    /// Test WebService
    /// </summary>
    [WebService(Namespace = "agileplus.xyz")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    [System.Web.Script.Services.ScriptService]

    
    public class PVGIS : System.Web.Services.WebService
    {
        
        public DataFormator TT = new DataFormator();
        
        private readonly string URL = "http://re.jrc.ec.europa.eu/pvgis5/MRcalc.php";
        private string lat;
        private string lon;
        private string status;
        private string error;
        public static Coordinate GetCoordinates(string region)
        {
            using (var client = new WebClient())
            {
                //region = string.Format("{0}", region.Replace(" ", "+"));
                string uri = "https://maps.googleapis.com/maps/api/geocode/json?address=" + region + "&key=AIzaSyCFGFo6xEbdAV6t5CGPg2brp3RU4fus7y4";

                string geocodeInfo = client.DownloadString(uri);
                JavaScriptSerializer oJS = new JavaScriptSerializer();
                GoogleGeoCodeResp latlongdata = oJS.Deserialize<GoogleGeoCodeResp>(geocodeInfo);

                if (latlongdata.status == "OK")
                {
                    return new Coordinate(latlongdata.results[0].geometry.location.lat, latlongdata.results[0].geometry.location.lng, latlongdata.status);
                }

                return new Coordinate(latlongdata.status, latlongdata.error_message);
            }
        }
        private void ConvertAddressToLatLon(string adrs)
        {
            Coordinate coordinate = new Coordinate();
            adrs = string.Format("{0}", adrs.Replace(",", "+"));
            coordinate = GetCoordinates(adrs);
            lat = coordinate.Latitude;
            lon = coordinate.Longitude;


            status = coordinate.Status;
            error = coordinate.Error;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        public void MSRV(string adr)
        {
            HttpConfiguration configuration = new HttpConfiguration();
            var corsAttr = new EnableCorsAttribute("*", "*", "*");
            configuration.EnableCors(corsAttr);
            HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");

            ConvertAddressToLatLon(adr);
                if (lat == null && lon == null)
                {
                    Context.Response.Write(status + ": " + error);
                }
                else
                {
                    var client = new RestClient(URL);
                    var request = new RestRequest(Method.GET);
                    request.AddHeader("Accept", "application/json");
                    request.AddParameter("lat", lat);
                    request.AddParameter("lon", lon);
                    request.AddParameter("horirrad", "1");
                    IRestResponse response = client.Execute(request);
                    var content = response.Content;
                    
                    if (response.IsSuccessful)
                    {
                        try
                        {
                            dynamic output = TT.FormatData(content);
                            var JSON = JsonConvert.SerializeObject(output);
                            Context.Response.Write(JSON);
                        }
                        catch(Exception e)
                        {
                            Context.Response.Write("Error Message: " + e.Message+ "\n Target: " + e.TargetSite);
                        }
                    }
                    else
                    {
                        Context.Response.Write("Error Message: " + response.ErrorMessage + "\n Error Exception" + response.ErrorException);
                    }
                }
            
        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        public void Sofinco(string p0, string q6, string cs, string i1, string n1, string c1, string c2, string m1, string m2, string m3, string a1, string am, string dm, string ne, string np)
        {
            HttpConfiguration configuration = new HttpConfiguration();
            var corsAttr = new EnableCorsAttribute("*", "*", "*");
            configuration.EnableCors(corsAttr);
            
            HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
            var URL = "https://r7.transcred.com/sofgate.asp?"+
                "p0="+p0+
                "&q6="+q6+
                "&cs="+cs+
                "&i1="+i1+
                "&n1="+n1+
                "&c1="+c1+
                "&c2="+c2+
                "&m1="+m1+
                "&m2="+m2+
                "&m3="+m3+
                "&a1="+a1+
                "&am="+am+
                "&dm="+dm+
                "&ne="+ne+
                "&np="+np;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(URL);
            
            request.Method = "POST";
            Encoding encoding = Encoding.GetEncoding("ISO-8859-1");
            StreamWriter requestWriter = new StreamWriter(request.GetRequestStream(), encoding);
            
            requestWriter.Close();
            try
            {
                WebResponse webResponse = request.GetResponse();
                Stream webStream = webResponse.GetResponseStream();
                StreamReader responseReader = new StreamReader(webStream,encoding);
                string response = responseReader.ReadToEnd();
                string xml = response;
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);
                string json = Newtonsoft.Json.JsonConvert.SerializeXmlNode(doc);
                Console.Write(xml + "\n");
                Context.Response.Write(json+",");
                responseReader.Close();
            }
            catch (Exception e)
            {
                Context.Response.Write("Error: " + e.Message + "\n" + e.StackTrace);
            }
        }
    }
}
