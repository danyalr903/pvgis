﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace Service
{
    public class DataFormator
    {
        public object FormatData(string apiStringData)
        {            
            string lattPattern = "(Latitude\\s\\(decimal\\sdegrees\\):)(\\t\\d+\\.*\\d*)";
            string longPattern = "(Longitude\\s\\(decimal\\sdegrees\\):)(\\t\\d+\\.*\\d*)";
            string radDbPattern = "(Radiation\\sdatabase\\:)(\\t)(PVGIS\\-CMSAF)";
            string osaPattern = "((19|20)\\d{2})(\\t\\t)([A-Z]+[a-z]*)(\\t\\t\\d+\\.*\\d*)";

            // Create the matches for the top-level data
            var lattitude = Regex.Match(apiStringData, lattPattern);
            var longitude = Regex.Match(apiStringData, longPattern);
            var radDb = Regex.Match(apiStringData, radDbPattern);

            // Create the result object, and populate the top-level properties
            ApiData apiObject = new ApiData();
            apiObject.Latitude = lattitude.Groups[2].ToString();
            apiObject.Latitude = apiObject.Latitude.Replace("\t", "");
            apiObject.Longitude = longitude.Groups[2].ToString();
            apiObject.Longitude = apiObject.Longitude.Replace("\t", "");
            apiObject.RadiationDatabase = radDb.Groups[3].ToString();

            // Split the sample data into an array 
            // to make it easier to enumerate what will become the nested data
            string[] apiArray = Regex.Split(apiStringData, "\r\n");

            // Step through it
            foreach (string s in apiArray)
            {
                var angle = Regex.Match(s, osaPattern, RegexOptions.IgnoreCase);
                if (angle.Success == true)
                {
                    // Create the properties
                    string year = angle.Groups[1].ToString();
                    string month = angle.Groups[4].ToString();
                    string hh = angle.Groups[5].ToString();
                    hh = hh.Replace("\t", "");
                    // Add to the collection
                    ApiSlopeAngle apiDate = new ApiSlopeAngle(year, month, hh);
                    apiObject.OptimalSlopeAngle.Add(apiDate);
                }
            }
            return apiObject;
        }
    }

    public class ApiData
    {
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string RadiationDatabase { get; set; }
        public List<ApiSlopeAngle> OptimalSlopeAngle { get; set; }

        public ApiData()
        {
            OptimalSlopeAngle = new List<ApiSlopeAngle>();
        }
    }

    public class ApiSlopeAngle
    {
        public string Year { get; set; }
        public string Month { get; set; }
        public string Hh { get; set; }
        public ApiSlopeAngle(string year, string month, string hh)
        {
            Year = year;
            Month = month;
            Hh = hh;
        }
    }
}