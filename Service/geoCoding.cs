﻿using System;
using System.Net;
using System.Web;
using System.Web.Script.Serialization;

namespace geoCoding
{
    public class GoogleGeoCodeResp
    {
        public string error_message { get; set; }
        public string status { get; set; }
        public results[] results { get; set; }
    }

    public class results
    {
        public string formatted_address { get; set; }
        public geometry geometry { get; set; }
        public string[] types { get; set; }
        public address_component[] address_components { get; set; }
    }

    public class geometry
    {
        public string location_type { get; set; }
        public location location { get; set; }
    }

    public class location
    {
        public string lat { get; set; }
        public string lng { get; set; }
    }

    public class address_component
    {
        public string long_name { get; set; }
        public string short_name { get; set; }
        public string[] types { get; set; }
    }


    public struct Coordinate
    {
        private string lat;
        private string lng;
        private string status;
        private string error;
        public Coordinate(string latitude, string longitude,string s)
        {
            lat = latitude;
            lng = longitude;
            status = s;
            error = null;
        }
        public Coordinate(string s,string e)
        {
            lat = null;
            lng = null;
            status = s;
            error = e;
        }
        public string Latitude { get { return lat; } set { lat = value; } }
        public string Longitude { get { return lng; } set { lng = value; } }
        public string Status { get { return status; } set { status = value; } }
        public string Error { get { return error; } set { error = value; } }
    }
    

}